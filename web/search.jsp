<%-- 
    Document   : search
    Created on : 1.6.2015, 11:24:35
    Author     : kl
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <%@ include file="head.jsp" %>
    <body>
       
    <%@ include file="menu.jsp" %>
    <div id="content">
    
            <div>
               
                <p id="sinfo"></p>
                <form  action="#" id="searchbox">
                    <input id="querytext" type="text" name="q" />
             
                <button id="dosearch">Hae</button>
                   
                </form> 
                <br />
               
                <h5>Koodit  <span id="rescount1"></span></h5>
                <div class="searchresults" id="resrows1"></div>
               
                <h5>Kysymykset  <span id="rescount2"></span></h5>
                <div class="searchresults" id="resrows2"></div>          
                  
                    
            </div>
    </div>
    </body>
</html>
