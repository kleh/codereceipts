
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.*,java.util.*"%>
<!DOCTYPE html>
<html>
    <%@ include file="head.jsp" %>
    <body>
        
    <%@ include file="menu.jsp" %>
    
    
      <div id="frontleft">
        <h2>Uusimmat koodit</h2>
                <%
                    // mene kontrollerin kautta
                    // jos tietoja ei ole ladattu
                    if (session.getAttribute("updated") == null){
                        response.sendRedirect("controller");
                    }
                    
                    // poistetaan updated tieto jotta index.jsp päivittyy jos painetaan refresh
                    session.removeAttribute("updated");
                    
                    List<Code> codes;
                    
                    if (session.getAttribute("codes") == null) {
                        out.print("Ei koodireseptejä");
                        return; 
                    }
                    codes = (List<Code>)session.getAttribute("codes");
                    
                    %>
                    <c:forEach items="${codes}" var="coder">
                        <div class="list">
                            <h3>${coder.id} ${coder.title}</h3>                          
                            <p>Kieli: ${coder.language}</p>
                            <p>Kuvaus: ${coder.description}</p>
                            <p>Luotu: ${coder.created}</p>
                            <p>Luonut: ${coder.createdby}</p>                                                       
                            <p>Kommentteja ${fn:length(coder.commentList)} kpl</p>
                            <p><a class="checkcode" href="code.jsp?id=${coder.id}">Tarkastele</a></p>
                            
                           
                        </div>
                            
                    </c:forEach>
    </div>
                    
    <div  id="frontright">
        <h2>Uusimmat kysymykset</h2>
                <%
                  
                    List<Question> questions;
                    
                    if (session.getAttribute("questions") == null) {
                        out.print("Ei kysymyksiä");
                        return; 
                    }
                    questions = (List<Question>)session.getAttribute("questions");                                                         
                    
                    %>
                    <c:forEach items="${questions}" var="qsn">
                        <div class="list">
                            <p>Kysymys ${qsn.question}</p>
                            <p>Luotu: ${qsn.created}</p>
                            <p>Luonut: ${qsn.createdby}</p>        
                             <p>Vastauksia ${fn:length(qsn.answerList)} kpl</p>
                            <p><a class="checkcode" href="answer.jsp?id=${qsn.id}">Tarkastele</a></p>
                                                       
                        </div>
                            
                    </c:forEach>
        </div>
    </body>
</html>
