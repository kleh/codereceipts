
package ejb;

import javax.ejb.*;
import java.util.*;
import entity.*;
import javax.persistence.*;

/**
 *
 * @author kl
 */
@Stateful
public class CodesEJB {
    
    @PersistenceUnit(unitName = "CodeReceiptsPU")
    EntityManagerFactory emf;
    EntityManager em = null;

    private void CreateEM(){
        if (this.em == null) {
            this.em = this.emf.createEntityManager();
            //em.setProperty("javax.persistence.cache.storeMode", "BYPASS");
        }
    }
    
    // Hae koodit tietokannasta, jos limit parametri > 0, palauta vain se määrä
    // order parametri määrittelee kentän jonka mukaan järjestetään
    public List getCodes(int limit, String order){
        this.CreateEM();
      
        Query query = this.em.createQuery("select c from Code c order by c." + order + " desc");
    
        if(limit > 0) query.setMaxResults(limit);
        
        List<Code> results = query.getResultList();
   
        return results;
    }
    
    // Hae kysymykset, sama logiikka kuin koodien kanssa
    public List getQuestions(int limit, String order){
        this.CreateEM();
       
        Query query = this.em.createQuery("select q from Question q order by q." + order + " desc");
        
        
        if(limit > 0) query.setMaxResults(limit);
        
        List<Question> results = query.getResultList();       
        
        return results;
    }
    
    // Lisää uuden koodin tietokantaan
    public String addCode(String title, String description, String language, String codetext, String createdby){
        String ret ;
        Code c = new Code();
        c.setTitle(title);
        c.setDescription(description);
        c.setLanguage(language);
        c.setCodetext(codetext);
        c.setCreatedby(createdby);    
        c.setCreated(new Date());
        
        if(c.getTitle().equals("")) {
            ret = "Koodilla tulee olla otsikko";
            return ret;
        }

        
        // Kaikki ok, tallennetaan            
        else{
            
            this.CreateEM();
                 
            try { 
                // JTAn takia ei voida käyttää itse tehtyä transaktiota.
                    this.em.joinTransaction(); // Liity JTA transaktioon  
                    this.em.persist(c);
                   
                    ret = "Koodi tallennettu"; 
                    this.em.flush();
                 
                    return ret;


            } catch (Exception e) {             
                e.printStackTrace();  
                ret = "Koodin tallennus epäonnistui"; 
                return ret;
            }
        }
              
    }
    // Hae koodeja merkkijonolla q
    // haetaan otsikosta, kielestä ja kuvauksesta
    public List searchCodes(String q){
        this.CreateEM();
        
        q = q.trim();       
        if(q.length() < 1){             
             return null;
         }
        
        
        Query query = this.em.createQuery("select c from Code c where lower(c.title) like ?1 "
                + " or lower(c.language) like ?2 "
                + " or lower(c.description) like ?3" + " or lower(c.createdby) like ?4");
        
        query.setParameter(1, "%" + q.toLowerCase() + "%");
        query.setParameter(2, "%" + q.toLowerCase() + "%");
        query.setParameter(3, "%" + q.toLowerCase() + "%");
        query.setParameter(4, "%" + q.toLowerCase() + "%");
         
        List<Code> results = query.getResultList();
        return results;
    }
    
    // haetaan otsikosta, kielestä ja kuvauksesta
    public List searchQuestions(String q){
        this.CreateEM();
        
        q = q.trim();       
        if(q.length() < 1){             
             return null;
         }
        
        
        Query query = this.em.createQuery("select q from Question q where lower(q.question) like ?1 "
                + " or lower(q.createdby) like ?2");
        
        query.setParameter(1, "%" + q.toLowerCase() + "%");
        query.setParameter(2, "%" + q.toLowerCase() + "%");
         
        List<Question> results = query.getResultList();
        return results;
    }
    
    // Hae yksi koodi id:n perusteella
    public Code getCodeById(long id){
        this.CreateEM();
      
        Query query = this.em.createQuery("select c from Code c where c.id = ?1 ");
        query.setParameter(1, id);
         
        List<Code> results = query.getResultList();
        try{
            Code c = results.get(0);
            return c;
        }
        catch(IndexOutOfBoundsException e){
            return null;
        }
        
    }
    
    // Hae yksi kysymys id:n perusteella
    public Question getQuestionById(long id){
        this.CreateEM();
      
        Query query = this.em.createQuery("select q from Question q where q.id = ?1 ");
        query.setParameter(1, id);
         
        List<Question> results = query.getResultList();
        try{
            Question q = results.get(0);           
            return q;
        }
        catch(IndexOutOfBoundsException e){
            return null;
        }
        
    }
    
    public String answerQuestion(String answer, String createdby, long question, long codeid){
        String res = "";
        
        // hae kysymys
        Question q = this.getQuestionById(question);
        if(q==null) return "Kysymys puuttuu";
        
        // Hae koodi jos liitetty (codeid >0)
        Code ca = null;
        if(codeid > 0){
            ca = this.getCodeById(codeid);
        }
        
        Answer aw = new Answer();
        aw.setAnswer(answer);
        aw.setQuestionId(q);
        aw.setCodeId(ca);
        aw.setCreatedby(createdby);
        aw.setCreated(new Date());
        
        this.CreateEM();
                 
        try { 
           // JTAn takia ei voida käyttää itse tehtyä transaktiota.
            this.em.joinTransaction(); // Liity JTA transaktioon  
            this.em.persist(aw);                   
            this.em.flush();
                 
            return "Tallennettu";
        } 
            catch (Exception e) {                             
            return "Tallennus ei onnistunut";
        }
        
      
    }
    
    // kommentin lisäys
    public boolean addComment(long codeid, String createdby, String comment){
        // tarkista että koodi löytyy
        Code c  = this.getCodeById(codeid);
        
        if(c==null) return false;
        
        Comment cm = new Comment();
        cm.setCodeId(c);
        cm.setComment(comment);
        cm.setCreatedby(createdby);
        cm.setCreated(new Date());
        
        this.CreateEM();
                 
        try { 
           // JTAn takia ei voida käyttää itse tehtyä transaktiota.
            this.em.joinTransaction(); // Liity JTA transaktioon  
            this.em.persist(cm);                   
            this.em.flush();                     
            
            return true;
        } 
            catch (Exception e) {                             
            return false;
        }
    }
    
        // Lisää uuden kysymyksen tietokantaan
    public String addQuestion(String question, String createdby){
        String ret ;
        Question q = new Question();
        q.setQuestion(question);
        q.setCreatedby(createdby);    
        q.setCreated(new Date());
        
        
        if(q.getQuestion().equals("")) {
            ret = "Kysymys puuttuu";
            return ret;
        }

        // Kaikki ok, tallennetaan            
        else{
            
            this.CreateEM();
                 
            try { 
                // JTAn takia ei voida käyttää itse tehtyä transaktiota.
                    this.em.joinTransaction(); // Liity JTA transaktioon  
                    this.em.persist(q);
                   
                    ret = "Kysymys tallennettu"; 
                    this.em.flush();
                 
                    return ret;


            } catch (Exception e) {             
                e.printStackTrace();  
                ret = "Kysymyksen tallennus epäonnistui"; 
                return ret;
            }
        }
              
    }
   
}
