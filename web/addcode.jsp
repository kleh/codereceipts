<%-- 
    Document   : index
    Created on : 27.5.2015, 10:11:54
    Author     : kl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ include file="head.jsp" %>
    <%@ include file="menu.jsp" %>
    <body>
        <div id="content">
            <h2>Uusi koodi</h2>
        
        <% String info = (String)session.getAttribute("info");
            if(info==null) info = "";
            
            // resetoi infotieto
            session.setAttribute("info","");
            
            %>
        <p class="info"><%= info %></p>
        <div id="form">
            <form id="addcodeform" action="controller" method="post">
                <input type="hidden" name="action" value="addcode" />
                <label>Otsikko
                    <input type="text"  id="ftitle" name="title" />
                </label>
                
                <label>Kieli
                    <select   id="flanguage" name="language">
                        <option selected="selected"></option>
                        <option>Python</option>
                        <option>Java</option>
                        <option>PHP</option>
                        <option>C</option>
                        <option>C++</option>
                        <option>Perl</option>
                        <option>Muu</option>
                    </select>
                </label><br />
                
                <label>Kuvaus</label><br />
                <textarea cols="60" rows="4" name="description"></textarea>
                <br />
                 <label>Koodi</label><br />
                <textarea class="codetext" cols="60" rows="20" name="codetext"></textarea>
                
                <br /><input type="submit" value="Tallenna" />
                
            </form>
        </div>
       </div>
    </body>
</html>
