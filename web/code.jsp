
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ include file="head.jsp" %>
    <body>
        <%@ include file="menu.jsp" %>
        <div id="content">
         
         
         <br />
         <h3><span id="onecodeid"></span> <span id="codetitle"></span>  </h3>
        
        <div class="list">                                                    
                            <p id="codelanguage"></p>
                            <p id="codedescription"></p>
                            <p id="codecreated"></p>
                            <p id="codecreatedby"></p>
                           
                            <div class="codetext"><p id="codetext" ></p> </div>                                                                                   
                            
        </div>
        
            <div id="commentblock">
            <p><h5>Kommentoi</h5>
            <p id="commentinfo"></p>
            <label>Nimi tai sähköposti: <input id="createdbycomment" type="text" value="${pageContext.request.userPrincipal.name}" /></label><br />

                <textarea id="codecomment" cols="40" rows="3"></textarea>
                                   <br /><button id="createcodecomment">Tallenna kommentti</button>
                               </p>
            </div>
            <div class="commentslist" id="commentslist"></div>
        </div>
       </div>
    </body>
</html>
