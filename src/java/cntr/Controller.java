/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cntr;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ejb.*;
import ejb.*;
import entity.*;
import java.util.*;

import javax.servlet.http.HttpSession;

/**
 *
 * @author kl
 */
public class Controller extends HttpServlet {

    @EJB
    CodesEJB cel;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           response.setContentType("text/html;charset=UTF-8");
        
        if(request.getParameter("action")==null){
           this.startPage(request, response);
        }
        else if(request.getParameter("action").equals("addcode")){
            this.addCode(request, response);
        }
        else if(request.getParameter("action").equals("addquestion")){
            this.addQuestion(request, response);
        }
        else if(request.getParameter("action").equals("answerquestion")){
            this.answerQuestion(request, response);
        }
        
   
    }

    private void startPage(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         HttpSession session = request.getSession(true);
         
         // result from ejb
         List<Code> codes =  cel.getCodes(5, "created");
         List<Question> questions = cel.getQuestions(5, "created");
                       
         
         // Aseta tieto että kutsu tuli tätä kautta
         session.setAttribute("updated", true);
         
         // Tuloslista ja ohjaus esityssivulle
         session.setAttribute("codes", codes);
         session.setAttribute("questions", questions);
         response.sendRedirect("index.jsp");
    }
        
    private void addCode(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);   
         
        String title = removeTag(request.getParameter("title").trim());
        String description = removeTag(request.getParameter("description").trim());
        String language = removeTag(request.getParameter("language").trim());
        String codetext = request.getParameter("codetext").trim();
        
        String username = request.getUserPrincipal().getName();
        String info;
        
       
        info = cel.addCode(title, description, language, codetext, username );
        
        session.setAttribute("info",info);           
        response.sendRedirect("addcode.jsp");
    }
    
    private void addQuestion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);   
         
        String question = removeTag(request.getParameter("question").trim());
        
        String username = request.getUserPrincipal().getName();
        String info;
        
       
        info = cel.addQuestion(question, username);
        
        session.setAttribute("info",info);           
        response.sendRedirect("addquestion.jsp");
    }
    
    private void answerQuestion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);   
         
        String answer = removeTag(request.getParameter("answer").trim());
        long codeid = Long.parseLong(request.getParameter("codeid"));
        long question = Long.parseLong(request.getParameter("question"));
        
        
        
        String username = request.getUserPrincipal().getName();
        String info;
        
       
        info = cel.answerQuestion(answer, username, question, codeid);
        
        session.setAttribute("info",info);           
        response.sendRedirect("answer.jsp?id=" + question);
    }
    
    // Poista html elementit
    private String removeTag(String txt){
        txt = txt.replaceAll("\\<.*?>","");
        txt = txt.replaceAll("&nbsp;","");
        txt = txt.replaceAll("&amp;","");
        return txt;
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
