
$(document).ready(function(){
    
    
    // Kun uusi koodi luodaan, tarkistetaan että otsikko on annettu
    $("#addcodeform").submit(function(e){
        e.preventDefault();
       var title = $("#ftitle").val().trim();
        if(title === undefined || title == ""){
            $("p.info").html("Otsikko on pakollinen");
            return false;
        }
        $("p.info").html();
        this.submit();
        return true;
    });
    
    // Hakutoiminto
    // Haetaan yhdellä pyynnöllä sekä koodeja että kysymyksiä
    // palauttaa json taulukon jossa indeksissä 0 on koodit taulukkona ja indeksissä 1 kysymykset
    $("#dosearch").click(function(e){
        e.preventDefault();
        
        
        var q = $("#querytext").val().trim();
        
        if(q.length < 3) {
            $("#sinfo").html("Hakusana pitää olla vähintään 3 merkkiä");
            return false;
        }
        else $("#sinfo").html("");
        
        $.get('webresources/coderws/search/' + q, 
            function(data, status){                
                
                // Koodit   
                var resh = "";
                var codedata = data[0];                    
                $("#rescount1").html(codedata.count);
                for(var i=0; i < codedata.count; i++){
                    resh = resh + "<div>" + 
                            "<p>" + codedata.data[i].id + 
                            " " + codedata.data[i].title + 
                            " " + codedata.data[i].created + 
                            " " + codedata.data[i].createdby + "</p>" + 
                            "<p>" + codedata.data[i].description + "</p>" + 
                            "<p><a class='checkcode' href='code.jsp?id=" +  codedata.data[i].id + "'>Tarkastele</a></p><hr /></div>";
                }                                                             
                $("#resrows1").html(resh);
                
                // Kysymykset                
                var qdata = data[1];
                var resh2 = "";
                $("#rescount2").html(qdata.count);
                for(var i=0; i < qdata.count; i++){
                    resh2 = resh2 + "<div>" + 
                            "<p>" + qdata.data[i].id + 
                            " " + qdata.data[i].question + 
                            " " + qdata.data[i].created + 
                            " " + qdata.data[i].createdby + "</p>" +                            
                            "<p><a class='checkcode' href='answer.jsp?id=" +  qdata.data[i].id + "'>Vastaa kysymykseen</a></p><hr /></div>";
                }                                                             
                $("#resrows2").html(resh2);
               
            });
       
    });
    
    // Lataa yksittäinen koodi
    // Hae id location objektin search parametrista
    $("#onecodeid").ready(function(e){
        var qid = window.location.search.split("=")[1];
        
        if(qid == 0 || qid === undefined) return;
        
        $.get('webresources/coderws/codes/' + qid,
            function(data,status){
                console.log(data); // debug...                                          
                $("#onecodeid").html(data.id);
                $("#codetitle").html(data.title);
                $("#codelanguage").html("Kieli: " + data.language);
                $("#codecreated").html(data.created);
                $("#codecreatedby").html(data.createdby);
                $("#codedescription").html(data.description);
                $("#codetext").html("<pre>" + data.codetext + "</pre>");
                
                var cmtext = "";
                
                for(var t=0;t < data.comments.length;t++){
                    var cm = data.comments[t];
                    cmtext = cmtext + "<hr /><p>" + cm.comment + "<br />" +
                            cm.created + "/" + cm.createdby + "</p>";
                }
                $("#commentslist").html(cmtext);
                
                
            });
    });
    
    
    // Koodin haku vastausta varten
    $("#dosearchcode").click(function(e){
        e.preventDefault();
          
        var q = $("#querytext").val().trim();
        
        if(q.length < 3) {           
            return false;
        }        
                
        
        $.get('webresources/coderws/search/' + q, 
            function(data, status){                
                
                // Koodit   
                var resh = "<option value='0'>-- Ei valittu --</option>";
                var codedata = data[0];                    
               
                for(var i=0; i < codedata.count; i++){
                    resh = resh +  
                            "<option value='" + codedata.data[i].id + "'>"
                            + codedata.data[i].title + 
                             " /" + codedata.data[i].created + 
                            " /" + codedata.data[i].createdby +
                            "</option>";
                        
                }
                resh = resh + "</select>";
                $("#codeans").html(resh);                
               
            });
       
    });
    
    // Valittu koodi vastaukseen
    $("#codeans").change(function(){
        var qid = $(this).val();  
        $("#codeid").val(qid);
        
        if (qid > 0){                                              
            $.get('webresources/coderws/codes/' + qid,
            function(data,status){
                $("#selcodedesc").html(
                        "<p>" + data.title + "/" + data.language + "</p>" +
                        "<p>" + data.description + "</p>" +
                        "<p>" + data.createdby + "/" + data.created + "</p>" +
                        "<p><a class='checkcode' href='code.jsp?id=" +  data.id + "'>Tarkastele</a></p>"
                        );
            });
        }
        else $("#selcodedesc").html("");
    });
    
    
  
       // Lataa yksittäinen kysymys
    // Hae id location objektin search parametrista
    $("#onequestionid").ready(function(e){
        var qid = window.location.search.split("=")[1];
        
        if(qid == 0 || qid === undefined) return;
        
        $.get('webresources/coderws/question/' + qid,
            function(data,status){
                console.log(data); // debug...                                          
                $("#onequestionid").html(data.id);
                $("#questiontext").html(data.question);
               
                $("#questioncreated").html(data.created + "/" + data.createdby);                                                          
                
                // aiemmat vastaukset
                var awtext = "";
                
                for(var t=0;t < data.answers.length;t++){
                    var am = data.answers[t];
                    awtext = awtext + "<hr /><p>" + am.answer + "<br />" +
                            am.created + "/" + am.createdby + "</p>" + 
                            // Jos koodiid > 0 näytä linkki muuten tyhjää
                            (am.codeid > 0 ? "<p><a class='checkcode' href='code.jsp?id=" +  am.codeid + "'>Koodi</a></p>": "");
                }
                $("#answerslist").html(awtext);
                
            });
    });
    
    // Vastaa kysymykseen
    $("#addanswerform").submit(function(e){
         e.preventDefault();
         
         var qid = window.location.search.split("=")[1];
         
         if(qid == 0 || qid === undefined) {
             $("p.info").html("Kysymys on pakollinen");
             return false;             
         };
         
         $("#questionid").val(qid);
         
       var answer = $("#fanswer").val().trim();
        if(answer === undefined || answer==""){
            $("p.info").html("Vastausteksti on pakollinen");
            return false;
        }
        $("p.info").html();
        this.submit();
        return true;
    });
    
    
    
    // tallenna kommentti
    $("#createcodecomment").click(function(e){
        var sd = {};
        sd.codeid = $("#onecodeid").html();
        sd.createdby = $("#createdbycomment").val();
        sd.comment = $("#codecomment").val();
        
        $.ajax({'type': 'POST',
            'url': 'webresources/coderws/codes/addcomment',
            'contentType': 'application/json',
            'data': JSON.stringify(sd),
            'dataType': 'json'})
                .always(function(data,status){
                    console.log(data);
                    $("#commentinfo").html(data.state);                                 
                    
                });
        
    });
    
    
});

