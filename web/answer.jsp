
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <%@ include file="head.jsp" %>
    <body>
        <%@ include file="menu.jsp" %>
        <div id="content">
         
        
        <h2>Kysymys <span id="onequestionid"></span>   </h2>
        
        <div class="list">                                                                               
                            <p id="questiontext"></p>
                            <p id="questioncreated"></p>                                             
        </div>
        <br />
        
        <% String info = (String)session.getAttribute("info");
            if(info==null) info = "";
            
            // resetoi infotieto
            session.setAttribute("info","");
            
            %>
        <p class="info"><%= info %></p>
        
        <h4>Hae vastaukseen liittyvä koodi</h4>
        <form  action="#" id="searchbox">
                    <input id="querytext" type="text" name="q" />
                <br />
                <button id="dosearchcode">Hae</button>
        </form>
        
        <div id="coderes"><select name="codeans" id="codeans"></select>
            <div class="list" id="selcodedesc"></div>
        </div>        
                
        <div id="form">
            <form id="addanswerform" action="controller" method="post">
                <input type="hidden" name="action" value="answerquestion" />
                <input type="hidden" id="codeid" name="codeid" value="0" />
               <input type="hidden" id="questionid" name="question" value="0" />
                
                <label>Vastausteksti, pakollinen (Vaatii kirjautumisen)</label><br />
                <textarea id="fanswer" cols="60" rows="8" name="answer"></textarea><br />                                
                <input type="submit" value="Tallenna" />
                       
            </form>
        </div>
        <h5>Aiemmat vastaukset</h5>
         <div class="commentslist" id="answerslist"></div>
       </div>
    </body>
</html>
