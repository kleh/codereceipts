/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.persistence.Transient;

/**
 *
 * @author kl
 */
@Entity
@Table(name = "CODE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Code.findAll", query = "SELECT c FROM Code c"),
    @NamedQuery(name = "Code.findById", query = "SELECT c FROM Code c WHERE c.id = :id"),
    @NamedQuery(name = "Code.findByTitle", query = "SELECT c FROM Code c WHERE c.title = :title"),
    @NamedQuery(name = "Code.findByLanguage", query = "SELECT c FROM Code c WHERE c.language = :language"),
    @NamedQuery(name = "Code.findByDescription", query = "SELECT c FROM Code c WHERE c.description = :description"),
    @NamedQuery(name = "Code.findByCreated", query = "SELECT c FROM Code c WHERE c.created = :created"),
    @NamedQuery(name = "Code.findByCreatedby", query = "SELECT c FROM Code c WHERE c.createdby = :createdby")})
public class Code implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "TITLE")
    private String title;
    @Size(max = 50)
    @Column(name = "LANGUAGE")
    private String language;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Lob
    @Column(name = "CODETEXT")
    private String codetext;
    @Column(name = "CREATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Size(max = 50)
    @Column(name = "CREATEDBY")
    private String createdby;
    @OneToMany(mappedBy = "codeId")
    private List<Answer> answerList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codeId")
    private List<Comment> commentList;
    
    
    public Code() {
    }

    public Code(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCodetext() {
        return codetext;
    }

    public void setCodetext(String codetext) {
        this.codetext = codetext;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    @XmlTransient
    public List<Answer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<Answer> answerList) {
        this.answerList = answerList;
    }

    @XmlTransient
    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Code)) {
            return false;
        }
        Code other = (Code) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Code[ id=" + id + " ]";
    }
    
}
