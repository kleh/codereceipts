<%-- 
    Document   : index
    Created on : 27.5.2015, 10:11:54
    Author     : kl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ include file="head.jsp" %>
    <body>
        <%@ include file="menu.jsp" %>
        <div id="content">

         
        <h2>Luo uusi Kysymys</h2>
        
        <% String info = (String)session.getAttribute("info");
            if(info==null) info = "";
            
            // resetoi infotieto
            session.setAttribute("info","");
            
            %>
        <p class="info"><%= info %></p>
        <div id="form">
            <form id="addquestionform" action="controller" method="post">
                <input type="hidden" name="action" value="addquestion" />
               
                
                <label>Kysymys</label><br />
                <textarea cols="60" rows="8" name="question"></textarea>
                
                
                <br /><input type="submit" value="Tallenna" />
                
            </form>
        </div>
       </div>
    </body>
</html>
