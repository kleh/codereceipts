/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import ejb.CodesEJB;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import java.util.*;
import entity.*;
import org.json.*;

/**
 * REST Web Service
 *
 * @author kl
 */

@Path("coderws")
public class CoderwsResource {
    @EJB
    CodesEJB cel;
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CoderwsResource
     */
    public CoderwsResource() {
    }

    /* Palauttaa kaikki koodit
     */
    @GET
    @Produces("application/json")
    @Path("/codes/all")
    public String getCodes() {
  
        List<Code> codes = cel.getCodes(0, "created");
        return this.genCodeListJSON(codes);
    }
    
    /* Koodien haku */
    @GET
    @Produces("application/json")
    @Path("/search/{query}")
    public String search(@PathParam("query") String query){
        List<Code> codes = cel.searchCodes(query);
        List<Question> questions = cel.searchQuestions(query);
        
        String codejson = this.genCodeListJSON(codes);
        String questionjson = this.genQuestionListJSON(questions);
        
        return "[" + codejson + "," + questionjson + "]";
    }
    
    // Hae yksittäinen koodi
    @GET
    @Produces("application/json")
    @Path("/codes/{cid}")
    public String getCodeById(@PathParam("cid") String cid){       
        Code code;
        try {
            code = cel.getCodeById(Long.parseLong(cid));
        }
        catch(Exception e){
            return "{'info':'error'}";
        }
        
        
        return this.genCodeJSON(code);
        
    }
    // Hae yksittäinen kysymys
    @GET
    @Produces("application/json")
    @Path("/question/{qid}")
    public String getQuestionById(@PathParam("qid") String qid){       
        Question question;
        try {
            question = cel.getQuestionById(Long.parseLong(qid));
        }
        catch(Exception e){
            return "{'info':'error'}";
        }        
        
        return this.genQuestionJSON(question);
        
    }
    
    // Tallenna kommentti
    // TODO: Pitäisikö estää rajapinnan käyttö suoraan ?
    @POST
    @Path("/codes/addcomment")
    @Consumes("application/json")
    @Produces("application/json")
    public String addComment(String content){
        String rt;
        JSONObject input;
        
        long codeid;
        String createdby;
        String comment;
        
        try {
            input = new JSONObject(content);
            codeid = input.getLong("codeid");
            createdby = this.removeTag(input.getString("createdby")).trim();
            comment = this.removeTag(input.getString("comment")).trim();
            
            boolean r = cel.addComment(codeid, createdby, comment);
                    
            if(createdby.equals("") || comment.equals(""))
                rt = "{\"state\":\"Tietoja puuttuu\",\"data\": " + content + "}"; 
            else if(r)
                rt = "{\"state\": \"Kommentti tallennettu\",\"data\": " + content + "}";
            else
                rt = "{\"state\":\"Kommentin tallennus ei onnistu\",\"data\": " + content + "}"; 
        }
        catch (JSONException e) {
            rt =  "{\"state\":\"Tiedonmuoto virhe\",\"data\": " + content + "}";
        }
        
        return rt;
    }
    
    // Apu metodi jolla generoidaan koodit tulosjoukosta koodit-JSON
    // JSON rakenne:
    /*
    {'count':y,
    'data': [code,code,code,code...]
    }
    */
    private String genCodeListJSON(List<Code> codes){
        JSONObject jm = new JSONObject();
        JSONArray jcodes = new JSONArray();
        JSONObject jc;
        String jsondata = "";       
        
        try{
            jm.put("count", codes.size());
           
            
            for(Code c: codes){
                jc = new JSONObject();
                jc.put("id", c.getId());
                jc.put("language", c.getLanguage());
                jc.put("title", c.getTitle());
                jc.put("description", c.getDescription());
                jc.put("created", c.getCreated());
                jc.put("createdby", c.getCreatedby());
                jc.put("codetext", c.getCodetext());
 
                jc.put("commentscount",c.getCommentList().size());
                jcodes.put(jc);
                }
                
            jm.put("data", jcodes);
            
        }
        catch(Exception e){
            return "{'info':'error'}";
        }
        
        return jm.toString();
    }
    
    // Kuten edellä mutta kysymykset
    private String genQuestionListJSON(List<Question> questions){
        JSONObject jm = new JSONObject();
        JSONArray jquestions = new JSONArray();
        JSONObject jq;
        String jsondata = "";       
        
        try{
            jm.put("count", questions.size());
           
            
            for(Question c: questions){
                jq = new JSONObject();
                jq.put("id", c.getId());
                jq.put("question", c.getQuestion());
            
                jq.put("created", c.getCreated());
                jq.put("createdby", c.getCreatedby());
                jquestions.put(jq);
                }
                
            jm.put("data", jquestions);
            
        }
        catch(Exception e){
            return "{'info':'error'}";
        }
        
        return jm.toString();
    }
    
    // kuten edellinen mutta vain yksi Code
    private String genCodeJSON(Code c){
        JSONObject jc;
        String jsondata = "";
        String ret = "";
        
        JSONObject comm;
        JSONArray comments;
        
        try{
                jc = new JSONObject();
                jc.put("id", c.getId());
                jc.put("language", c.getLanguage());
                jc.put("title", c.getTitle());
                jc.put("description", c.getDescription());
                jc.put("created", c.getCreated());
                jc.put("createdby", c.getCreatedby());
                jc.put("codetext", c.getCodetext());
                
                comments = new JSONArray();
                for(Comment cm: c.getCommentList()){
                    comm = new JSONObject();
                    comm.put("created",cm.getCreated());
                    comm.put("createdby",cm.getCreatedby());
                    comm.put("comment",cm.getComment());   
                    comments.put(comm);
                }
                
                jc.put("comments", comments);
                ret = jc.toString();
        }
        catch(Exception e){
            ret = "{'info':'error'}";
        }
        
        return ret;
    }
    
     // Yksi kysymys ja sen vastaukset
    private String genQuestionJSON(Question q){
        JSONObject jq;
        String jsondata = "";
        String ret = "";
        JSONObject ans;
        JSONArray answers;
    
        try{
                jq = new JSONObject();
                jq.put("id", q.getId());
                jq.put("question", q.getQuestion());
                jq.put("created", q.getCreated());
                jq.put("createdby", q.getCreatedby());
                
                answers = new JSONArray();
                for(Answer aw: q.getAnswerList()){
                    ans = new JSONObject();
                    ans.put("created",aw.getCreated());
                    ans.put("createdby",aw.getCreatedby());
                    ans.put("answer",aw.getAnswer());
                    
                    if(aw.getCodeId() != null)
                        ans.put("codeid", aw.getCodeId().getId()); // kytketty koodi
                    else ans.put("codeid",0);
                    
                    answers.put(ans);
                }
                
                jq.put("answers", answers);
                
                ret = jq.toString();
        }
        catch(Exception e){
            ret = "{'info':'error'}";
        }
        
        return ret;
    }
    
        // Poista html elementit
    private String removeTag(String txt){
        txt = txt.replaceAll("\\<.*?>","");
        txt = txt.replaceAll("&nbsp;","");
        txt = txt.replaceAll("&amp;","");
        return txt;
    }
   
}
